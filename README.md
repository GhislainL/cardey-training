*Looking for a shareable component template? Go here --> [sveltejs/component-template](https://github.com/sveltejs/component-template)*

# [Cardey Training](https://ghislainl.gitlab.io/cardey-training/)


[Svelte-spa-router](https://github.com/ItalyPaleAle/svelte-spa-router)

[Meta Tags](https://megatags.co/) for social media tags generation and validation

[Optimizilla](https://imagecompressor.com/) for image optimization
