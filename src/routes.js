// import {wrap} from 'svelte-spa-router'

import Home from './routes/Home.svelte'
import About from './routes/About.svelte'
import NotFound from './routes/NotFound.svelte'
import Exercices from './routes/Exercices.svelte'

const routes = {
    '/': Home,
    '/about': About,
    '/exercices': Exercices, 
    // Catch-all
    // This is optional, but if present it must be the last
    '*': NotFound,
}

export default routes